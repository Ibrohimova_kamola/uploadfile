import axios from "axios";

export const uploadFile = async (file: File) => {
    const formData = new FormData()
    formData.append("tenantId", "test")
    formData.append("module", "test")
    formData.append("fileName", "test")
    formData.append("file", file)

    return await axios.post("http://192.168.100.241:9999/api/file/upload/public", formData)

}